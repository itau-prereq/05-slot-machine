package br.com.itau;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Sorteador {
    private static List<SlotEnum> Slot = Arrays.asList(SlotEnum.values());

    private static final int SIZE = Slot.size();
    private static final Random RANDOM = new Random();

    public static SlotEnum sorteiaSlot()  {
        return Slot.get(RANDOM.nextInt(SIZE));
    }
}
